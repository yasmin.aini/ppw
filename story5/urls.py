from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('jadwal', views.add, name='jadwal'),  
    path('show/',views.show, name='show'),  
    path('delete/<str:id>', views.destroy),  

]
