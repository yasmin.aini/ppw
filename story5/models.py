from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.PositiveIntegerField(default=100)
    semester = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=100)
    ruangan= models.CharField(max_length=100)

    def __str__(self):
        return self.nama_matkul
