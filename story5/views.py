from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import MataKuliah
from .forms import matkulForm





# Create your views here.

def add(request):  
    data = MataKuliah.objects.all() 
    form = matkulForm()
    if request.method == "POST":  
        form = matkulForm(request.POST)  
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/show')  
            except:  
                pass  
    else:  
        form = matkulForm()

    response = {
        'form': form,
        'data' : data,
        
    }  
    #print(response)
    return render(request,'jadwal.html', response)  


def show(request):  
   jadwal = MataKuliah.objects.all()  
   return render(request,"hasiljadwal.html",{'jadwal':jadwal})  

def destroy(request, id):  
    jadwal = MataKuliah.objects.get(id=id)  
    jadwal.delete()  
    return redirect("/show")  
