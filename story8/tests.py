from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views


# Create your tests here.

class UnitTestForStory8(TestCase):
    def test_response_page(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'index2.html')

