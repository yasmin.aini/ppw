from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib import auth
from .apps import Story9Config
import json

class TestStory9(TestCase):
    def test_login(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 302)

    def test_url_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_setup(self):
        self.client = Client()
        self.response = self.client.get('/story9/')
        self.page_content = self.response.content.decode('utf8')


    def test_failed_logging_in(self):
        userInput = {'username' : 'testing', 'password' : 'testing'}
        response = Client().post('/story9/api/v1/login/', json.dumps(userInput), content_type="application/json")
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'status': 401,
            'message': 'Username does not exist'}
        )

    def test_badreq_api(self):
        response = Client().get('/story9/api/v1/login/')
        self.assertEqual(response.status_code, 400)

        response = Client().get('/story9/api/v1/signup/')
        self.assertEqual(response.status_code, 400)

    def test_app(self):
        self.assertEqual(Story9Config.name, "story9")




    
    
