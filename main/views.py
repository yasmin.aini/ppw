from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    return render(request, 'main/index.html')

def kontak(request):
    return render(request, 'main/kontak.html')

def skills(request):
    return render(request, 'main/skills.html')
