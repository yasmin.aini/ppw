from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def story1(request):
    return render(request, 'story1.html')
